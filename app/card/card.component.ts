import { Component, Input } from '@angular/core';

import { Task } from '../model/task';

@Component({
    moduleId: module.id,
    selector: 'app-card',
    templateUrl: 'card.component.html',
    styleUrls: ['card.component.css']
})
export class CardComponent {
    @Input() task: Task;

    statusToggle() {
        this.task.completed = !this.task.completed;
    }

    deleteNote() {
        this.task.deleted = true;
    }

    //Tried to come up with a way top search through the tasks index but was unable to figure it out.  Angular is hard.   
    /*
    withinIndex() {
        var a = tasks.indexOf(this.task.content);
        alert(  );
        alert(tasks.indexOf(this.task.content));
        return tasks.indexOf(this.task.content);
    }
    */

}

